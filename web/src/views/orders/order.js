import { handleException } from "../../utils"

const getOrders = async () => {
  const orders = await fetch('/api/orders')
  return await orders.json()
}

const getOrderCount = async () => {
  const count = await fetch('/api/orders/count')
  return await count.json()
}

const filterOrders = (orders, searchTerm) => {
  if (searchTerm.length === 0) {
    return orders
  }
  return orders.filter(order => {
    return order.description.toLowerCase().includes(searchTerm)
        || order.id.toLowerCase().includes(searchTerm)
        || order.note.toLowerCase().includes(searchTerm)
  })
}


export {
  getOrders,
  getOrderCount,
  filterOrders,
}