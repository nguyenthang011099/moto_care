import { handleException } from "../../utils"

const getShops = async () => {
  const shops = await fetch('/api/shops')
  return await shops.json()
}

const getShopsCount = async () => {
  const count = await fetch('/api/shops/count')
  return await count.json()
}


const createShop = async (shopData) => {
  console.log(shopData)
  const response = await fetch('/api/shops', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(shopData)
  })
  return handleException(response)
}

const editShop = async (shopId, shopData) => {
  const response = await fetch(`/api/shops/${shopId}`, {
    method: 'PATCH',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(shopData)
  })
  return handleException(response)

}

const deleteShop = async (shopId) => {
  const response = await fetch(`/api/shops/${shopId}`, {
    method: 'DELETE',
  })
  return handleException(response)
}

const filterShops = (shops, searchTerm) => {
  if (searchTerm.length === 0) {
    return shops
  }
  return shops.filter(shop => {
    return shop.email.toLowerCase().includes(searchTerm)
      || shop.fullname.toLowerCase().includes(searchTerm)
      || shop.mobile.toLowerCase().includes(searchTerm)
  })
}

export {
  getShops,
  getShopsCount,
  createShop,
  editShop,
  deleteShop,
  filterShops
}