import { handleException } from "../../utils"

const getProduct = async () => {
  const product = await fetch('/api/product')
  console.log(888, product)
  return await product.json()
}

const getProductCount = async () => {
  const count = await fetch('/api/product/count')
  return await count.json()
}

const getProductAvailabilityCount = async () => {
  const availability_count = await fetch('/api/product/availability-count')
  return await availability_count.json()
}

const createProduct = async (productData) => {
  const response = await fetch('/api/product', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body:  JSON.stringify(productData)
  })
  return handleException(response)
}

const editProduct = async (productData) => {
  const response = await fetch(`/api/product/${productData.id}`, {
    method: 'PATCH',
    headers: {
      'Content-Type': 'application/json'
    },
    body:  JSON.stringify(productData)
  })
  return handleException(response)
}

const deleteProduct = async (productID) => {
  await fetch(`/api/product/${productID}`, {
    method: 'DELETE'
  })
}

const filterProduct = (
  product,
  selectedCategory,
  selectedEmployee, 
  selectedSerialNumber
) => {
  const matchedCategory = new Set(product.filter(product => {
    if (selectedCategory === undefined) {
      return product
    } else {
      return product.category_id.category === selectedCategory
    }
  }))

  const matchedSerialNumber = new Set(product.filter(product => {
    if (selectedSerialNumber === '') {
      return product
    } else {
      return product.serialnumber.toLowerCase().includes(selectedSerialNumber.toLowerCase())
    }
  }))

  const result = new Set([...matchedCategory]
    .filter(product => matchedSerialNumber.has(product)))

  return result
}

export {
  getProduct,
  getProductCount,
  getProductAvailabilityCount,
  createProduct,
  editProduct,
  deleteProduct,
  filterProduct
}