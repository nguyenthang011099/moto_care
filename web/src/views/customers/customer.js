import { handleException } from "../../utils"

const getCustomers = async () => {
  const customers = await fetch('/api/customers')
  return await customers.json()
}

const getCustomersCount = async () => {
  const count = await fetch('/api/customers/count')
  return await count.json()
}

const getCustomerByName = async (nickname) => {
  const customer = await fetch(`/api/customers/${nickname}`)
  return await customer.json()
}

const createCustomer = async (customerData) => {
  console.log(customerData)
  const response = await fetch('/api/customers', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(customerData)
  })
  return handleException(response)
}

const editCustomer = async (customerId, customerData) => {
  const response = await fetch(`/api/customers/${customerId}`, {
    method: 'PATCH',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(customerData)
  })
  return handleException(response)

}

const deleteCustomer = async (customerId) => {
  const response = await fetch(`/api/customers/${customerId}`, {
    method: 'DELETE',
  })
  return handleException(response)
}

const filterCustomers = (customers, searchTerm) => {
  if (searchTerm.length === 0) {
    return customers
  }
  return customers.filter(customer => {
    return customer.email.toLowerCase().includes(searchTerm)
      || customer.fullname.toLowerCase().includes(searchTerm)
      || customer.mobile.toLowerCase().includes(searchTerm)
      || customer.nickname.toLowerCase().includes(searchTerm)
  })
}

export {
  getCustomers,
  getCustomerByName,
  getCustomersCount,
  createCustomer,
  editCustomer,
  deleteCustomer,
  filterCustomers
}