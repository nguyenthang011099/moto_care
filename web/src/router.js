import { createRouter, createWebHistory } from 'vue-router'
import Dashboard from './views/Dashboard.vue'

const routes = [
  { 
    path: '/',
    name: 'Dashboard',
    component: Dashboard 
  },
  { 
    path: '/equipment',
    name: 'Equipment',
    component: () => import('./views/equipment/Equipment.vue') 
  },
  { 
    path: '/employees',
    name: 'Employees',
    component: () => import('./views/employees/Employees.vue')
  },
  {
    path: '/customers',
    name: 'Customers',
    component: () => import('./views/customers/Customers.vue')
  },
  {
    path: '/shops',
    name: 'Shops',
    component: () => import('./views/shops/Shops.vue')
  },
  {
    path: '/orders',
    name: 'Orders',
    component: () => import('./views/orders/Orders.vue')
  },
  {
    path: '/product',
    name: 'Product',
    component: () => import('./views/product/Product.vue')
  }
]

const router = createRouter({
    history: createWebHistory(),
    routes
})

export default router

