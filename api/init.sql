CREATE TABLE equipment (
    id SERIAL PRIMARY KEY,
    serialnumber TEXT UNIQUE NOT NULL,
    name TEXT NOT NULL,
    category_id INT,
    employee_id INT NOT NULL
)

CREATE TABLE employees (
    id SERIAL PRIMARY KEY,
    nickname TEXT UNIQUE NOT NULL,
    fullname TEXT NOT NULL,
    mobile TEXT UNIQUE,
    email TEXT UNIQUE,
    activestatus BOOLEAN DEFAULT TRUE
)

CREATE TABLE categories (
    id SERIAL PRIMARY KEY,
    category TEXT UNIQUE NOT NULL
)

CREATE TABLE product (
    id SERIAL PRIMARY KEY,
    serialnumber TEXT UNIQUE NOT NULL,
    name TEXT NOT NULL,
    description TEXT,
    category_id INT,
    shop_id INT NOT NULL,
    price TEXT,
    brand TEXT
)

CREATE TABLE orders (
     id SERIAL PRIMARY KEY,
     description TEXT,
     shop_id INT NOT NULL,
     customer_id INT NOT NULL,
     product_id INT NOT NULL,
     total_price TEXT,
     note TEXT,
     created_at TIMESTAMP DEFAULT NOW()
)

CREATE TABLE shops (
    id SERIAL PRIMARY KEY,
    fullname TEXT NOT NULL,
    mobile TEXT UNIQUE,
    created_at TIMESTAMP DEFAULT NOW(),
    activestatus BOOLEAN DEFAULT TRUE,
    address TEXT
)

CREATE TABLE customers (
    id SERIAL PRIMARY KEY,
    fullname TEXT NOT NULL,
    mobile TEXT UNIQUE,
    activestatus BOOLEAN DEFAULT TRUE
)