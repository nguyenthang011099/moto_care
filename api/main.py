from fastapi import FastAPI

from routers import employees, equipment, categories, product, customers, shops, orders

router = FastAPI()

@router.get('/api/')
def root():
    return {'message': 'Hello World'}

router.include_router(categories.router)
router.include_router(customers.router)
router.include_router(employees.router)
router.include_router(equipment.router)
router.include_router(product.router)
router.include_router(shops.router)
router.include_router(orders.router)
