import peewee
from peewee import fn

from fastapi import APIRouter, HTTPException 

from playhouse.shortcuts import model_to_dict

from models.customers import Customers 

from pydantic import BaseModel

router = APIRouter()

class Customer(BaseModel):
    activestatus: bool
    fullname: str
    mobile: str

class CustomerID(Customer):
    id: int

@router.get('/api/customers')
def get_all_customers():
    customers = Customers.select().order_by(Customers.activestatus.desc())
    customers = [model_to_dict(customer) for customer in customers]
    return customers

@router.get('/api/customers/count')
def get_customers_count():
    count = Customers.select().count()
    return count
    

@router.post('/api/customers', response_model=Customer)
def create_customer(payload_: Customer):
    try:
        payload = payload_.dict()
        customer = Customers.create(**payload)
        return model_to_dict(customer)
    except peewee.IntegrityError as e:
        error_message = str(e)
        if 'customers_email_key' in error_message:
            raise HTTPException(status_code=400, detail='email already exists')
        if 'customers_mobile_key' in error_message:
            raise HTTPException(status_code=400, detail='mobile number already exists')
        

@router.patch('/api/customers/{id}')
def edit_customer(id :int, payload_: Customer):
    try:
        payload = payload_.dict()
        formatted_nickname = payload['nickname'].lower().strip()
        exist = Customers.select().where(fn.LOWER(Customers.nickname) == formatted_nickname).exists()
        selected_customer = Customers.get_by_id(id)
        if exist and selected_customer.nickname != formatted_nickname:
            raise HTTPException(status_code=400, detail='nickname already exists')
        else:
            (Customers.update( 
                activestatus = payload['activestatus'],
                nickname = payload['nickname'],
                fullname = payload['fullname'],
                mobile = payload['mobile'],
                email = payload['email'])
            .where(Customers.id == id)
            .execute())
    except peewee.IntegrityError as e:
        error_message = str(e)
        if 'customers_email_key' in error_message:
            raise HTTPException(status_code=400, detail='email already exists')
        if 'customers_mobile_key' in error_message:
            raise HTTPException(status_code=400, detail='mobile number already exists')

@router.delete('/api/customers/{id}')
def delete_customer(id: int):
    try:
        customer = Customers.get_by_id(id)
        customer.delete_instance()
    except peewee.IntegrityError as e:
        error_message = str(e)
        if 'equipment_customer_id_fkey' in error_message:
            raise HTTPException(status_code=400, detail=f'{customer.nickname} is borrowing something')
        
        