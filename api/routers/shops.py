import peewee
from peewee import fn

from fastapi import APIRouter, HTTPException 

from playhouse.shortcuts import model_to_dict

from models.shops import Shops 

from pydantic import BaseModel

router = APIRouter()

class Shop(BaseModel):
    activestatus: bool
    nickname: str
    fullname: str
    mobile: str
    email: str

class ShopID(Shop):
    id: int

@router.get('/api/shops')
def get_all_shops():
    shops = Shops.select().order_by(Shops.activestatus.desc())
    shops = [model_to_dict(shop) for shop in shops]
    return shops

@router.get('/api/shops/count')
def get_shops_count():
    count = Shops.select().count()
    return count

@router.get('/api/shops/{nickname}', response_model=ShopID)
def get_shop_by_nickname(nickname: str):
    shop = Shops.get(Shops.nickname == nickname)
    return model_to_dict(shop)
    

@router.post('/api/shops', response_model=Shop)
def create_shop(payload_: Shop):
    try:
        payload = payload_.dict()
        formatted_nickname = payload['nickname'].lower().strip()
        exist = Shops.select().where(fn.LOWER(Shops.nickname) == formatted_nickname).exists()
        if exist:
            raise HTTPException(status_code=400, detail='nickname already exists')
        else:
            shop = Shops.create(**payload)
            return model_to_dict(shop)           
    except peewee.IntegrityError as e:
        error_message = str(e)
        if 'shops_email_key' in error_message:
            raise HTTPException(status_code=400, detail='email already exists')
        if 'shops_mobile_key' in error_message:
            raise HTTPException(status_code=400, detail='mobile number already exists')
        

@router.patch('/api/shops/{id}')
def edit_shop(id :int, payload_: Shop):
    try:
        payload = payload_.dict()
        formatted_nickname = payload['nickname'].lower().strip()
        exist = Shops.select().where(fn.LOWER(Shops.nickname) == formatted_nickname).exists()
        selected_shop = Shops.get_by_id(id)
        if exist and selected_shop.nickname != formatted_nickname:
            raise HTTPException(status_code=400, detail='nickname already exists')
        else:
            (Shops.update( 
                activestatus = payload['activestatus'],
                nickname = payload['nickname'],
                fullname = payload['fullname'],
                mobile = payload['mobile'],
                email = payload['email'])
            .where(Shops.id == id)
            .execute())
    except peewee.IntegrityError as e:
        error_message = str(e)
        if 'shops_email_key' in error_message:
            raise HTTPException(status_code=400, detail='email already exists')
        if 'shops_mobile_key' in error_message:
            raise HTTPException(status_code=400, detail='mobile number already exists')

@router.delete('/api/shops/{id}')
def delete_shop(id: int):
    try:
        shop = Shops.get_by_id(id)
        shop.delete_instance()
    except peewee.IntegrityError as e:
        error_message = str(e)
        if 'equipment_shop_id_fkey' in error_message:
            raise HTTPException(status_code=400, detail=f'{shop.nickname} is borrowing something')
        
        