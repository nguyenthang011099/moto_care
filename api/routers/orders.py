from pkg_resources import AvailableDistributions
from fastapi import APIRouter, HTTPException

from typing import Optional

from playhouse.shortcuts import model_to_dict

from peewee import fn, JOIN
import peewee

from pydantic import BaseModel

from models.orders import Orders
from models.customers import Customers
from models.shops import Shops
from models.product import Product
from models import db

router = APIRouter()

class Order(BaseModel):
    note: str
    total_price: str
    customer: int
    shop: int
    product: int
    description: str

@router.get('/api/orders')
def get_all_orders():
    orders = Orders.select()
    order = [model_to_dict(order) for order in orders]
    return order

@router.get('/api/orders/count')
def get_orders_count():
    count = Orders.select().count()
    return count