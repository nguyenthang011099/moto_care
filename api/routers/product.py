from pkg_resources import AvailableDistributions
from fastapi import APIRouter, HTTPException

from typing import Optional

from playhouse.shortcuts import model_to_dict

from peewee import fn, JOIN

from pydantic import BaseModel

from models.product import Product
from models.categories import Categories
from models import db

router = APIRouter()

class Product1(BaseModel):
    serialnumber: str
    name: str
    category: int
    shop: int
    description: str
    brand: str
    price: str

@router.get('/api/product')
def get_all_product():
    products = Product.select().order_by(fn.LOWER(Product.name))
    products = [model_to_dict(product) for product in products]
    return products

@router.get('/api/product/count')
def get_product_count():
    count = Product.select().count()
    return count


@router.post('/api/product')
def create_product(payload_: Product1):
    payload = payload_.dict()
    formatted_serialnumber = payload['serialnumber'].strip().upper()
    exist = Product.select().where(Product.serialnumber == formatted_serialnumber).exists()
    if exist:
        raise HTTPException(status_code=400, detail='Product serial number already exists')
    else:
        payload['serialnumber'] = formatted_serialnumber 
        product = Product.create(**payload)
        return model_to_dict(product)

@router.patch('/api/product/{id}')
def update_product(id: int, payload_: Product1):
    payload = payload_.dict()
    formatted_serialnumber = payload['serialnumber'].strip().upper()
    exist = Product.select().where(Product.serialnumber == formatted_serialnumber).exists()
    selected_product = Product.get_by_id(id)
    if exist and selected_product.serialnumber != formatted_serialnumber:
        raise HTTPException(status_code=400, detail='Product serial number already exists')
    else:
        payload['serialnumber'] = formatted_serialnumber
        (Product.update(
            serialnumber = payload['serialnumber'],
            name = payload['name'],
            category_id = payload['category_id'],
            shop_id = payload['shop_id'],
            price = payload['price'],
            description = payload['description'])
        .where(Product.id == id)
        .execute())       

@router.delete('/api/product/{id}')
def delete_product(id: int):
    product = Product.get_by_id(id)
    product.delete_instance()
        