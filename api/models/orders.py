from . import PeeWeeBaseModel
import peewee as p
from models.shops import Shops
from models.customers import Customers
from models.product import Product

class Orders(PeeWeeBaseModel):
    id = p.PrimaryKeyField()
    customer = p.ForeignKeyField(Customers, backref="orders")
    shop = p.ForeignKeyField(Shops, backref="orders")
    product = p.ForeignKeyField(Product, backref="orders")
    note = p.TextField()
    description = p.TextField()
    total_price = p.TextField()
