import datetime
from . import PeeWeeBaseModel
import peewee as p 

class Shops(PeeWeeBaseModel):
    id = p.PrimaryKeyField()
    fullname = p.TextField(null = False)
    mobile = p.TextField(unique = True)
    address = p.TextField
    activestatus = p.BooleanField(default = True)
    created_at = p.DateTimeField(default=datetime.datetime.now())