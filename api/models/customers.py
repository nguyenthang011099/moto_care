from . import PeeWeeBaseModel
import peewee as p

class Customers(PeeWeeBaseModel):
    id = p.PrimaryKeyField()
    fullname = p.TextField(null = False)
    mobile = p.TextField(unique = True)
    activestatus = p.BooleanField(default = True)
